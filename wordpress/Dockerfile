FROM php:7-apache

MAINTAINER Alex Treitjak <a.treitjak@gmail.com>

ENV DB_DRIVER=mysql
ENV DB_NAME=wordpress
ENV DB_HOST=db
ENV DB_PORT=3306
ENV DB_USER=root
ENV DB_PASS=root
ENV WOOCOMMERCE_VERSION 2.5.5
ENV WORDPRESS_VERSION 4.5.3
ENV WORDPRESS_SHA1 835b68748dae5a9d31c059313cd0150f03a49269

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
        libxml2-dev \
        libicu-dev \
        libxslt-dev \
        mysql-client \
        supervisor \
        wget \
        git \
        cron \
        unzip \
        libbz2-dev \
        curl \
        openssh-server \
    && docker-php-ext-install iconv pdo_mysql zip xml bz2 xsl mcrypt mysqli soap intl opcache mbstring \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-bz2=/usr/include/ \
    && docker-php-ext-install gd

COPY php.ini /usr/local/etc/php
COPY docker-php-pecl-install /usr/local/bin/
RUN chmod 755 /usr/local/bin/docker-php-pecl-install

RUN docker-php-pecl-install xdebug-2.4.1

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

# Setup xdebug
RUN echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/docker-php-pecl-xdebug.ini
RUN echo "xdebug.remote_autostart=0" >> /usr/local/etc/php/conf.d/docker-php-pecl-xdebug.ini
RUN echo "xdebug.remote_connect_back=1" >> /usr/local/etc/php/conf.d/docker-php-pecl-xdebug.ini
RUN echo "xdebug.remote_port=9000" >> /usr/local/etc/php/conf.d/docker-php-pecl-xdebug.ini
RUN echo "xdebug.remote_host=192.168.254.1" >> /usr/local/etc/php/conf.d/docker-php-pecl-xdebug.ini

RUN mkdir /var/run/sshd
RUN echo 'root:root' | chpasswd
RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
RUN echo "export VISIBLE=now" >> /etc/profile

ADD supervisord-web.conf /etc/supervisor/conf.d/web.conf
ADD supervisord-cron.conf /etc/supervisor/conf.d/cron.conf
ADD supervisord-ssh.conf /etc/supervisor/conf.d/ssh.conf

RUN a2enmod rewrite

RUN curl -o wordpress.tar.gz -SL https://wordpress.org/wordpress-${WORDPRESS_VERSION}.tar.gz \
	&& echo "$WORDPRESS_SHA1 *wordpress.tar.gz" | sha1sum -c - \
	&& tar -xzf wordpress.tar.gz -C /usr/src/ \
	&& rm wordpress.tar.gz \
	&& chown -R www-data:www-data /usr/src/wordpress

#woocommerce
RUN wget https://downloads.wordpress.org/plugin/woocommerce.$WOOCOMMERCE_VERSION.zip -O /tmp/temp.zip \
    && cd /usr/src/wordpress/wp-content/plugins \
    && unzip /tmp/temp.zip \
    && rm /tmp/temp.zip

# Download WordPress CLI
RUN curl -L "https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar" > /usr/bin/wp && \
    chmod +x /usr/bin/wp

# Additional tools
ADD https://getcomposer.org/composer.phar /usr/local/bin/composer
ADD https://phar.phpunit.de/phpcpd.phar   /usr/local/bin/phpcpd
ADD https://phar.phpunit.de/phpdcd.phar   /usr/local/bin/phpdcd
ADD https://phar.phpunit.de/phploc.phar   /usr/local/bin/phploc
ADD https://static.phpmd.org/php/latest/phpmd.phar   /usr/local/bin/phpmd
ADD https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar   /usr/local/bin/phpcs
ADD http://codeception.com/codecept.phar   /usr/local/bin/codecept
ADD http://get.sensiolabs.org/php-cs-fixer.phar   /usr/local/bin/php-cs-fixer

RUN cd /usr/local/bin && \
  chmod +x phpcpd phpdcd phploc phpcs phpmd codecept composer php-cs-fixer

COPY start /entrypoint.sh
RUN chmod 755 /entrypoint.sh

RUN \
    apt-get -y autoremove && \
    apt-get -y clean && \
    rm -rf /var/lib/apt/lists/*

VOLUME /var/www/html

EXPOSE 80 22 9000

CMD ["/entrypoint.sh"]
