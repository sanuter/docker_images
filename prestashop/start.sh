#!/bin/bash

wget -O /tmp/prestashop.zip "https://www.prestashop.com/download/old/prestashop_"$PS_VERSION".zip"
mkdir /tmp/prepare
unzip -q /tmp/prestashop.zip -d /tmp/prepare

if [ $PS_INSTALL = 1 ]
 then

    if [ -f "/tmp/prepare/prestashop.zip" ]
    then
        mkdir /tmp/prepare/prestashop
        unzip -q /tmp/prepare/prestashop.zip -d /tmp/prepare/prestashop
        cp -R /tmp/prepare/prestashop/* $PS_ROOT_PATH
    else
        cp -R /tmp/prepare/prestashop/* $PS_ROOT_PATH
    fi

    mv -vf $PS_ROOT_PATH/install $PS_ROOT_PATH/install-dev
    mv -vf $PS_ROOT_PATH/admin $PS_ROOT_PATH/admin-dev

  if [ -f $PS_ROOT_PATH/install-dev/index_cli.php ]
  then
    echo "Installing ..."
    php $PS_ROOT_PATH/install-dev/index_cli.php --domain="$PS_BASE_URL" --db_server="$PS_DB_HOST" --db_name="$PS_DB_NAME" --db_user="$PS_DB_USER" \
     --db_password="$PS_DB_PASSWORD" --country="us" --firstname="$PS_ADMIN_FIRSTNAME" --lastname="$PS_ADMIN_LASTNAME" --email="$PS_ADMIN_EMAIL" --password="$PS_ADMIN_PASSWORD"

    chown www-data:www-data -R $PS_ROOT_PATH
  fi
fi

exec supervisord -n